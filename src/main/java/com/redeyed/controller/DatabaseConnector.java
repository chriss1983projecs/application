package com.redeyed.controller;

import com.redeyed.annotations.AnnotationManager;
import com.redeyed.database.RedeyedCoreManager;
import com.redeyed.database.pojos.DataBaseConfig;
import com.redeyed.database.utils.JSONMapper;
import com.redeyed.model.Player;
import com.redeyed.view.TestClass;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseConnector {
    List<Class> entitys;

    final static Logger logger = Logger.getLogger(TestClass.class);

    @Autowired
    public DatabaseConnector(RedeyedCoreManager redeyedCoreManager) {
        logger.info("DatabaseConnector is started");
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(Config.class);
//        v.addActionHandler(new ButtonHandler());
        entitys = new ArrayList<>();

        entitys.add(com.redeyed.entity.User.class);

        prepareDatabase(redeyedCoreManager);
    }

    private void prepareDatabase(RedeyedCoreManager redeyedCoreManager) {
        String username = "root";
        String password = "Ift9h2vC";
        String databaseURL = "jdbc:mysql://redeyed.de:3306/test";
        String dialect = "MySQLDialect";
        String driver = "com.mysql.jdbc.Driver";

        DataBaseConfig config;

        String databaseConfig;

        databaseConfig = "{\"username\":\"" + username + "\",\"password\":\"" + password + "\",\"databaseURL\":\""
                + databaseURL + "\",\"dialect\":\"" + dialect + "\",\"driver\":\"" + driver + "\"}";
        config = JSONMapper.createDatabaseObject(databaseConfig);

        config.toString();

        redeyedCoreManager.initDatabaseConnection(databaseConfig, AnnotationManager.getAnnotatedClassed("com.redeyed"));

        Player p = redeyedCoreManager.getEntity(Player.class, 37);
        System.out.println(p.toString());
    }
}
