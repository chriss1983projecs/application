package com.redeyed.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.redeyed.database, com.redeyed.view, com.redeyed.controller")
public class Config {
}
